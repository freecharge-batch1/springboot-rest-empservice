package com.freecharge.empservice.controller;

import com.freecharge.empservice.entity.Employee;
import com.freecharge.empservice.exception.EmployeeAlreadyExistsException;
import com.freecharge.empservice.exception.EmployeeNotFoundException;
import com.freecharge.empservice.service.EmployeeService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    ResponseEntity<?> responseEntity;
    /*
     GET -> All employees
     /employees

     GET -> Specific Employee
     /employees/3

     POST -> Create a new Employee
     /employees

     PUT -> update en employee
     /employees

     DELETE -> delete an employee
     /employees/3


     */

    @PostMapping("/employees")
    public ResponseEntity<?> saveEmployee(@RequestBody Employee employee) throws EmployeeAlreadyExistsException{
       try {
           responseEntity = new ResponseEntity<>(this.employeeService.saveEmployee(employee),HttpStatus.CREATED);
       } catch(EmployeeAlreadyExistsException e){
           throw e;
       } catch(Exception e){
           responseEntity = new ResponseEntity<>("some internal error occured. Please try again!!", HttpStatus.INTERNAL_SERVER_ERROR);
       }

       return responseEntity;
    }

    @GetMapping("/employees")
    public ResponseEntity<?> getAllEmployees(){
        try {
            responseEntity = new ResponseEntity<>(this.employeeService.getEmployeeList(), HttpStatus.OK);
        }
            catch(Exception e){
            responseEntity = new ResponseEntity<>("some internal error occured. Please try again!!", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return responseEntity;
    }

    @GetMapping("/employees/{id}")
    public ResponseEntity<?> getEmployeeById(@PathVariable("id") int id) throws EmployeeNotFoundException{
        try {
            responseEntity = new ResponseEntity<>(this.employeeService.getEmployeeById(id),HttpStatus.OK);
        } catch(EmployeeNotFoundException e){
            throw e;
        } catch(Exception e){
            responseEntity = new ResponseEntity<>("some internal error occured. Please try again!!", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @PutMapping("/employees")
    public ResponseEntity<?> updateEmployee(@RequestBody Employee employee) throws EmployeeNotFoundException{
        try {
            responseEntity = new ResponseEntity<>(this.employeeService.updateEmployee(employee),HttpStatus.OK);
        } catch(EmployeeNotFoundException e){
            throw e;
        } catch(Exception e){
            responseEntity = new ResponseEntity<>("some internal error occured. Please try again!!", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return responseEntity;
    }

    @PutMapping("/employees/updateEmail")
    public ResponseEntity<?> updateEmpEmail(@RequestBody Employee employee) throws EmployeeNotFoundException {
        try {
            responseEntity = new ResponseEntity<>(this.employeeService.updateEmployeeEmail(employee.getId(), employee.getEmail()), HttpStatus.OK );
        } catch (EmployeeNotFoundException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            responseEntity = new ResponseEntity<>("some internal error occured. Please try again!!", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return responseEntity;
    }
}
