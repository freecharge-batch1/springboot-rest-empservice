package com.freecharge.empservice.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "address_details")
@Data
@NoArgsConstructor
public class Address {

    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private int id;
    private String houseNo;
    private String street;
    private String city;
    private int zipCode;

    @ManyToMany(mappedBy = "addressList")
    @JsonIgnore
    private List<Employee> employeeList;
}
