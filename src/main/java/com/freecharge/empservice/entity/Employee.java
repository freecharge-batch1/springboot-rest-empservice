package com.freecharge.empservice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@Table(name = "emp_details")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) /* This is database specific */
    @Column(name = "emp_id")
    private int id;
    @Column(name = "emp_name")
    private String name;
    @Column(name = "emp_email")
    private String email;
    @Column(name = "emp_contactNo")
    private String contactNo;

    /*
    * Target Entity -> Employee
    * Associated Entity -> Address
    * */
   /* @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="add_id")
    private Address address;*/

    /*
    OneToMany
     */
   /* @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "emp_address", joinColumns = {
            @JoinColumn(name = "emp_id", referencedColumnName = "emp_id")
    }, inverseJoinColumns = {
            @JoinColumn(name = "address_id", referencedColumnName = "id")
    })*/

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "emp_address", joinColumns = {
            @JoinColumn(name = "emp_id", referencedColumnName = "emp_id")
    }, inverseJoinColumns = {
            @JoinColumn(name = "address_id", referencedColumnName = "id")
    })

  //  emp_id->id     address_id -> id
    private List<Address> addressList;




}
