package com.freecharge.empservice.repository;

import com.freecharge.empservice.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
@Transactional
public interface EmployeeRepository extends JpaRepository<Employee,Integer> {
    /*
        By default EmployeeRepository implements all the
        CRUD methods on Employee entity
     */
    /*
    select * from emp_details where id = 1
    select * from emp_details where name = 'emp1' and id=1
     */

    Optional<Employee> findByName(String name);

    /*
    Query Methods
     */

    @Modifying
    @Query("update Employee e set e.email=?2 where e.id=?1")
    void updateEmpEmail(int id, String email);
}
