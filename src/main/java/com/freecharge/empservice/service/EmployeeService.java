package com.freecharge.empservice.service;


import com.freecharge.empservice.entity.Employee;
import com.freecharge.empservice.exception.EmployeeAlreadyExistsException;
import com.freecharge.empservice.exception.EmployeeNotFoundException;
import com.freecharge.empservice.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService implements IEmployeeService{

    @Autowired
    EmployeeRepository employeeRepository;

    @Override
    public Employee saveEmployee(Employee employee) throws EmployeeAlreadyExistsException {
        Optional<Employee> empById = this.employeeRepository.findByName(employee.getName());
        if(empById.isPresent())
            throw new EmployeeAlreadyExistsException();
        else
            return this.employeeRepository.save(employee);
    }

    @Override
    public Employee getEmployeeById(int id) throws EmployeeNotFoundException {
        Employee employee = null;
        Optional<Employee> empById = this.employeeRepository.findById(id);

        if(!empById.isPresent())
            throw new EmployeeNotFoundException();
        else
            employee = empById.get();

        return employee;
    }

    @Override
    public List<Employee> getEmployeeList() {
        return this.employeeRepository.findAll();
    }

    @Override
    public Employee updateEmployee(Employee employee) throws EmployeeNotFoundException {
        Optional<Employee> empById = this.employeeRepository.findById(employee.getId());
        if(!empById.isPresent())
            throw new EmployeeNotFoundException();
        else
            return this.employeeRepository.save(employee);
    }

    @Override
    public boolean deleteEmployee(int id) throws EmployeeNotFoundException {
        return false;
    }

    @Override
    public Employee updateEmployeeEmail(int id, String email) throws EmployeeNotFoundException {
        this.employeeRepository.updateEmpEmail(id,email);
        return this.getEmployeeById(id);
    }
}
