package com.freecharge.empservice.service;

import com.freecharge.empservice.entity.Employee;
import com.freecharge.empservice.exception.EmployeeAlreadyExistsException;
import com.freecharge.empservice.exception.EmployeeNotFoundException;

import java.util.List;

public interface IEmployeeService {

    //business methods

    public Employee saveEmployee(Employee employee) throws EmployeeAlreadyExistsException;
    public Employee getEmployeeById(int id) throws EmployeeNotFoundException;
    public List<Employee> getEmployeeList();
    public Employee updateEmployee(Employee employee) throws EmployeeNotFoundException;
    public boolean deleteEmployee(int id) throws EmployeeNotFoundException;

    public Employee updateEmployeeEmail(int id,String email) throws EmployeeNotFoundException;

    /*
    1. get employee details by id (Employee )
    2. employeeRepository.delete(employee);
    3. "deletedSuccessfully", HttpStatus.OK
    4. employee object, HttpStatus.OK
     */
}
